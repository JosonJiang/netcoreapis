using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using joson;

namespace netcore5.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserInfoController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Joson", "蒋寒鹏", "Tom", "Jim", "Tony", "田永章", "任志强", "赵孟頫", "颜真卿", "田英章"
        };

        private readonly ILogger<UserInfoController> _logger;

        public UserInfoController(ILogger<UserInfoController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<userinfo> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new userinfo
            {
                id = rng.Next(-20, 55),
                name = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
