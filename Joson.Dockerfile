#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

# 这是一个调试编译过程用的Dockerfile

FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build
WORKDIR /src
COPY ["Solution.sln", "./"]
# COPY ["gitlabcicd/*.csproj", "gitlabcicd/"]
# COPY ["joson/*.csproj", "joson/"]
# COPY ["netcore5/*.csproj", "netcore5/"]


COPY ["*/*.csproj", "./"]
RUN for file in $(ls *.csproj); do mkdir -p ${file%.*}/ && mv $file ${file%.*}/; done

# #调试信息部分
RUN pwd && ls
# #调试信息部分



# # #调试信息部分
# RUN pwd && ls && cd ./ && pwd && ls
# RUN pwd && ls && cd joson && pwd && ls
# RUN pwd && ls && cd netcore5 && pwd && ls
# # #调试信息部分

# # Restore 指定项目
# # 非必要 restore "joson/joson.csproj" 因为 netcore5.csproj 中引用其
# # RUN dotnet restore "joson/joson.csproj" 
RUN dotnet restore "netcore5/netcore5.csproj"

# # 等同上指令 切换工作目录 Restore 完毕再切换回 src 去 ，减少语句生成更小镜像
# WORKDIR /src/netcore5
# RUN dotnet restore
# WORKDIR /src


# # #调试信息部分
# RUN pwd && ls && cd joson && pwd && ls && cd obj && pwd && ls
# RUN pwd && ls && cd netcore5 && pwd && ls && cd obj && pwd && ls
# # #调试信息部分

# # Restore作用：主要是寻找当前目录下的项目文件（project.json）
# # 然后利用NuGet库还原整个项目的依赖库，然后遍历每个目录，生成项目文件，继续还原该项目文件中的依赖项。
# # Restore整个解决方案，需要拷贝所有项目
# RUN dotnet restore
COPY . .


WORKDIR "/src/joson"
RUN dotnet build -c Release -o /app/build

WORKDIR "/src/netcore5"
RUN dotnet build "netcore5.csproj" -c Release -o /app/build



FROM build AS publish
RUN dotnet publish "netcore5.csproj" -c Release -o /app/publish


FROM base AS final
WORKDIR /srv/webapi
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "netcore5.dll"]